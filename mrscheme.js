/* entry point for MrScheme */

var MrScheme = { 
    type: "MrScheme",
    onChange: function(editor) {
        $("#eval").hide();
        if(MrScheme.unmark!=null) {
            MrScheme.unmark();
            MrScheme.unmark = null;
        }
	// notify the gtk based frontend if any
	if (typeof(MrSchemeDesktop)!= "undefined") {
		MrSchemeDesktop.codeChanged();
	}
    },
    editorOnKey: function(editor,key) {
        if(key.keyCode!=key.DOM_VK_ENTER && key.keyCode!=key.DOM_VK_RETURN) {
            return false;
        }
        if(!key.ctrlKey) {
            return false;
        }

        if(key.type=="keydown") {
            menuExecute(editor)(null);
            key.stop();
            return true; 
        } else {
            key.stop();
            return true;
        }     
    },
    quickEvalOnKey : function(editor,key) {
        if(key.DOM_VK_ENTER === undefined) {
            key.DOM_VK_ENTER = 14;
        }
        if(key.DOM_VK_RETURN === undefined) {
            key.DOM_VK_RETURN = 13;
        }
        if(key.keyCode!=key.DOM_VK_ENTER && key.keyCode!=key.DOM_VK_RETURN) {
            return false;
        }
        if(key.shiftKey) {
            return false;
        }

        if(key.type=="keydown") {
            quickEvaluate(editor, false);
            key.stop();
            return true; 
        } else {
            key.stop();
            return true;
        }
    },
    splitterActive : false,
    MIN_EDITOR_HEIGHT : 400,
    MR_SCHEME_STARTED : false,
    traceId : 0
};


function startConsole() {
    //alert("start console");
    try {
    if(window.console==null) {
        //alert("pas de console");
        console = { log: function() { /* alert("log"); */ } };
    } else {
        //alert("console presente");
        if(console.log==null) {
            //alert("pas de log");
            console.log = function() { /* alert("log"); */ } 
        }
    }
    } catch(e) { alert("exception"+e); };
}

// entry point
$(document).ready(function(){

    if(MrScheme.MR_SCHEME_STARTED) {
        alert(M$("Unknown exception raised (please report)").toString());
        return false;
    }

    MrScheme.MR_SCHEME_STARTED = true;

    startConsole();
    console.log("start");

    // install the tooltips
    TT.setTips();

    // translations
    Message101.frenchTranslations();
    Message101.Language = 'fr';

    // modifier les tooltips
    $('.tooltip').each(function(index) {
        var element = $(this);//.children().first();
	if (Message101.Language != 'fr') {
	    Message101.frenchTranslations();
	    Message101.Language = 'fr';
	}
        var msg = M$(element.html());
        //console.log("tooltip message",msg,msg.toString());
        element.html(msg.toString());
    });

    // modifier les valeurs de boutons
    $("#Eval  > .buttonLabel").text(M$('Evaluate'));
    $("#Trace > .buttonLabel").text(M$('Trace'));
    $("#Steps > .buttonLabel").text(M$('Steps'));

    // install codemirror editor
    var editor = CodeMirror.fromTextArea(document.getElementById("editor101"),
                                         {
                                             lineNumbers : true,
                                             matchBrackets : true,
                                             tabMode : "indent",
                                             onChange: MrScheme.onChange,
                                             onKeyEvent : MrScheme.editorOnKey
                                         });
    editor.setValue("");
    MrScheme.editor = editor;

    MrScheme.MIN_EDITOR_HEIGHT = $('#editor .CodeMirror').height();

    // install quick eval editor
    var quickEval =  CodeMirror.fromTextArea(document.getElementById("quickEval"),
                                             {
                                                 lineNumbers : false,
                                                 gutter : true,
                                                 matchBrackets : true,
                                                 tabMode : "indent",
                                                 onKeyEvent : MrScheme.quickEvalOnKey
                                             });
    quickEval.setValue("");
    quickEval.setMarker(1,">");
    MrScheme.quickEval = quickEval;
    $('#eval').hide();


    // menu callbacks
    $('#New').click(menuNew(editor));
    $('#Import').click(menuImport(editor));
    //$('#fileLoader').click(function(evt) { $(this).show(); });
    $('#Export').click(menuExport(editor));
    $('#Help').click(function() { menuHelp(editor); });
    $('#Undo').click(function() { editor.undo(); });
    $('#Redo').click(function() { editor.redo(); });
    $('#Execute').click(menuExecute(editor));
    $('#Eval').click(function() { quickEvaluate(quickEval, false); });
    $('#Trace').click(function() { quickEvaluate(quickEval, "calls"); });
    $('#Steps').click(function() { quickEvaluate(quickEval, "all"); });

    // splitter
    $('#splitter').mousedown(function(evt) {
        // console.log("mouse down");        
        if(!MrScheme.splitterActive) {
            evt.stopPropagation();
            MrScheme.splitterActive = true;
            MrScheme.splitterY = evt.pageY;
            $(document).bind('mousemove.SplitterEvents',function(evt) {
                // console.log("mouse moved",MrScheme.splitterActive);
                if(MrScheme.splitterActive) {
                    evt.stopPropagation();
                    var height = $('#editor .CodeMirror').height();
                    var diff = evt.pageY - MrScheme.splitterY;                
                    var nheight = height + diff;
                    //console.log("height",height,"new height",nheight);
                    if(nheight>MrScheme.MIN_EDITOR_HEIGHT) {
                        $("#editor .CodeMirror-scroll").height(nheight);
                        //$("#editor .CodeMirror").height(nheight);
                    }
                    MrScheme.splitterY = evt.pageY;
                    return false;
                }
            }).bind('mouseup.SplitterEvents', function(evt) {
                // console.log("mouse up");        
                if(MrScheme.splitterActive) {
                    evt.stopPropagation();
                    $(document).unbind('.SplitterEvents');
                    MrScheme.splitterActive = false;
                    return false;
                }
            });
            return false;
        }
    }).hover(function() {
        $(this).css('border','solid 1px gray');
    }, function() {
        $(this).css('border','solid 1px lightgray');
    });
    
});

$(window).resize(function(){
    TT.setTips();
});

function outputln(html) {
    $('<div class="output">').append(html)
        .append('</div>')
        .appendTo('#console');
    //alert("window height="+$(window).height());
    //window.scrollTo(0,$(window).height());
}

function traceln(divid, html) {
    $('<div class="trace">').append(html)
        .append('</div>')
        .appendTo(divid);
    //alert("window height="+$(window).height());
    //window.scrollTo(0,$(window).height());
}

function errorln(expr) {
    $('<div class="output">').append(expr.toHTML())
        .append('</div>')
        .click(function(evt) {
            MrScheme.editor.setCursor(expr.startPos.lpos-1, expr.startPos.cpos-1);
            MrScheme.editor.focus();
        })
        .hover(function(evt) {
            $(this).find('.error').addClass("clickable");
        }, function(evt) {
            $(this).find('.error').removeClass("clickable");
        })
        .appendTo('#console');
    MrScheme.unmark = MrScheme.editor.markText({line: expr.startPos.lpos-1, ch: expr.startPos.cpos-1},
                                               {line: expr.endPos.lpos-1, ch: expr.endPos.cpos-1},
                                               "error");
    //alert("window height="+$(window).height());
    window.scrollTo(0,$(document).height());
}

function quickErrorln(expr) {
    $('<div class="output">').append(expr.toHTML())
        .append('</div>')
        .appendTo('#console');
}

function menuExecute(editor) {
    return function(evt) {

        if(MrScheme.unmark!=null) {
            MrScheme.unmark();
            MrScheme.unmark = null;
        }
        
        $("#Execute img")
            .fadeOut(
                'fast',
                function() {
                    $(this).attr("src","./media101/ftendturn.png")
                        .fadeIn(
                            'fast',
                            function() {
                                $("#console").html("");
                                
                                outputln('<span class="exec">'+M$("Program execution started").toString()+'</span>');
                                var startTime = new Date().getTime();

                                // 1) parsing phase
                                outputln('<span class="exec">'+M$("  ==> Parsing expression #").toString()+'<span id="parseNumber">0</span></span>');
                
                                var prog = editor.getValue();
                                var lexer = new Tokenizer(prog);
                                var parser = new Parser(lexer);
                                
                                var exprs = new Array();
                                var cont = true;
                                do {
                                    var expr = parser.parseNext();
                                    if(expr.type=="parseError") {
                                        errorln(expr);
                                        TT.setTips();
                                        $("#Execute img").fadeOut('fast',function() { $(this).attr("src","./media101/ftkexi.png").fadeIn('fast') });
                                        return;
                                    } else if(expr.type!="unit") {
                                        exprs.push(expr);
                                        $('#parseNumber').text(""+exprs.length);
                                    } else {
                                        cont = false;
                                    }
                                } while(cont);
                                
                                // 2) evaluation phase
                                var penv = defaultPrimsEnv();
                                NumericalTowerLib.installPrimEnv(penv);

                                TreeLib.installPrimEnv(penv);
                                CanvasLib.installPrimEnv(penv);
                                var evaluator = new Evaluator(penv);
                                MrScheme.evaluator = evaluator;
                                outputln('<span class="exec">'+M$("  ==> Evaluating expression #").toString()+'<span id="evalNumber">0</span></span>');
                                
                                for(var i=0;i<exprs.length;i++) {
                                    var expr = exprs[i];
                                    $('#evalNumber').text(""+(i+1)+"/"+exprs.length);
                                    
                                    var result = null;
                                    try {
                                        if(expr.type!="define" && expr.type!="test") {
                                            var exprId = "__expr"+i;
                                            outputln("<pre class='evalCode' id='"+exprId+"'>&gt; "+expr.toString()+"</pre>");
                                            var fclick = function(expr,i) {
                                                $('#'+exprId).click( function(evt) {
                                                    var pos = expr.startPos.clone();
                                                    var lpos = pos.lpos-1;
                                                    var cpos = pos.cpos-1;
                                                    //console.log("expr",i,expr.startPos,"jump to line",lpos,"character",cpos);
                                                    editor.setCursor({line:lpos, ch:cpos});
                                                    editor.focus();
                                                });
                                            };
                                            if(expr.startPos!=null) {
                                                fclick(expr,i);
                                            }
                                        }
                                        result = evaluator.eval(expr, false);
                                    } catch(exc) {
                                        result = new EvalError(M$("Evaluation aborpted (termination problem ?)").toString() + "<br>(exception: " + exc + ")",expr);
                                    }
                                    if(result.type=="evalError") {
                                        errorln(result);
                                        TT.setTips();
                                        $("#Execute img").fadeOut('fast',function() { $(this).attr("src","./media101/ftkexi.png").fadeIn('fast') });
                                        return;
                                    } else if(result.type!="unit") {
                                        //alert("value = "+result);
                                        outputln(result.toHTML());
                                        if(result.afterOutput!=null) {
                                            result.afterOutput();
                                        }
                                    }
                                }
                                
                                var endTime = new Date().getTime();
                                var duration = endTime - startTime;
                                //console.log("duration",duration);
                                TT.setTips();
                                outputln('<span class="exec">'+M$("Program execution finished (elapsed time $0 ms)",""+duration).toString()+'</span>');
                                if(evaluator.nbTestsPassed>0) {
                                    outputln('<span class="exec">'+M$("  ==> All $0 tests passed",""+evaluator.nbTestsPassed).toString()+'</span>');
                                }
                                
                                $("#Execute img").fadeOut('fast',function() { $(this).attr("src","./media101/ftkexi.png").fadeIn('fast') });
                                
                                
                                $("#eval").show();
                                MrScheme.quickEval.refresh();
                                //MrScheme.quickEval.focus();
                                window.scrollTo(0,$(document).height());
                            })
                })
    }
}

function quickEvaluate(editor,traceMode) {
    if(MrScheme.evaluator==null) {
        var penv = defaultPrimsEnv();
        TreeLib.installPrimEnv(penv);
        CanvasLib.installPrimEnv(penv);
        MrScheme.evaluator = new Evaluator(penv);
    }
    // install tracer, if required
    if(traceMode) {
        MrScheme.traceId++;
        var divid = "consoleTrace" + MrScheme.traceId;
        MrScheme.evaluator.traceln = function(html) { traceln("#"+divid, html); };
        MrScheme.evaluator.evalDepth = 0;
        MrScheme.evaluator.traceMode = traceMode;
        $("<div class='consoleTrace' id='"+divid+"'>").append('</div>').appendTo("#console");
    } else {
        MrScheme.evaluator.traceln = null;
        MrScheme.evaluator.evalDepth = -1;
        MrScheme.evaluator.traceMode = false;
    }
        
    var evaluator = MrScheme.evaluator;

    // 1) parsing phase
    var prog = editor.getValue();
    var lexer = new Tokenizer(prog);
    var parser = new Parser(lexer);
    
    var exprs = new Array();
    var cont = true;
    do {
        var expr = parser.parseNext();
        if(expr.type=="parseError") {
            outputln("<pre class='evalCode'>&gt; "+editor.getValue()+"</pre>");
            quickErrorln(expr);
            TT.setTips();
            editor.setValue("");
            return;
        } else if(expr.type!="unit") {
            exprs.push(expr);
        } else {
            cont = false;
        }
    } while(cont);

    // 2) evaluation phase
    for(var i=0;i<exprs.length;i++) {
        var expr = exprs[i];
        var result = null;
        try {
            result = evaluator.eval(expr, traceMode);
        } catch(exc) {
            result = new EvalError(M$("Evaluation aborpted (termination problem ?)").toString() + "<br>(exception: " + exc + ")",expr);
        }
        if(result.type=="evalError") {
            outputln("<pre class='evalCode'>&gt; "+expr.toString()+"</pre>");
            quickErrorln(result);
            TT.setTips();
            editor.setValue("");
            return;
        } else {
            //alert("value = "+result);
            outputln("<pre class='evalCode'>&gt; "+expr.toString()+"</pre>");
            if(result.type!="unit") {
                outputln(result.toHTML());
                if(result.afterOutput!=null) {
                    result.afterOutput();
                }
            }
        }
    }
    TT.setTips();
    editor.setValue("");
    //editor.focus();
    window.scrollTo(0,$(document).height());
}


function menuNew(editor) {
    return function(evt) {
	if (typeof(MrSchemeDesktop)!= "undefined") {
		MrSchemeDesktop.newFile();
		return;
	}
        apprise(M$("Confirm edition of a new program ?").toString(), {'verify':true, 'textYes':M$("Yes").toString(), 'textNo':M$("No").toString()}, function(r)
                {
                    if(r)
                    { 
                        editor.setValue("");
                        GLOBAL_MENU_EXPORT.currentFileName = null;
                        $('#MrFileName').fadeOut('slow', function() {
                            $(this).text('');
                            MrScheme.editor.focus();
                        });
                    }
                    else
                    { 
                        return
                    }
                });
    };
}

GLOBAL_MENU_EXPORT = { currentFileName: null };

// Warning: HACK !
function is_browser_chrome() {
    return navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
}

function menuExport(editor) {
    var f = function(evt) {
	    	
	if (typeof(MrSchemeDesktop)!= "undefined") {
		MrSchemeDesktop.saveFile();
		return;
	}

        if (BlobBuilder && saveAs) {
            if(GLOBAL_MENU_EXPORT.currentFileName === null) {
                var flag_input = is_browser_chrome();
                var msg = ""
                if (flag_input) {
                    msg = M$("Save the program (example: myprog.scm)").toString();
                } else {
                    msg = M$("Export the program (then save as...)").toString();
                }

                apprise(msg, {'textOk':M$("Ok").toString(), 'textCancel':M$("Cancel"), 'input':flag_input},
                function(r) {
                    if(r) { // ok with file name
                        GLOBAL_MENU_EXPORT.currentFileName = r;
                        $('#MrFileName').fadeOut('slow', function() {   
                            $(this).html('<tt>'+r+'</tt>');
                            $(this).fadeIn('slow');
                        });
                        return fileExport(r)
                    } else { // cancel
                        return
                    }
                });
            } else { // existing current file name
                return fileExport(GLOBAL_MENU_EXPORT.currentFileName)
            }
        } else { // no save as functionality
        
            apprise(M$("Save the program after export").toString(), {'textOk':M$("Ok").toString()}, function(r) { 
                var nwindow = window.open("export.scm","_blank","",false);

                var html = "<html><head><title>export.scm</title></head><body><pre>";
                for(var i=0;i<editor.lineCount();i++) {
                    var line = editor.getLine(i);
                    html+=editor.getLine(i)+"<br>";
                }
                html+="<br></pre></body></html>";
            
                //nwindow.document.title("export.scm");
                nwindow.document.open();
                nwindow.document.write(html);
                nwindow.document.close();
                nwindow.document.title = "export.scm";
            });
        }
    };
    return f;
}

function menuImport(editor) {
    return function(evt) {
	if (typeof(MrSchemeDesktop)!= "undefined") {
		MrSchemeDesktop.loadFile();
	} else {
        	// Check for the various File API support.
	        if (window.File && window.FileReader && window.FileList) {
       		     // Great success! All the required File APIs are supported.
	        } else {
        	    apprise(M$('Your Internet browser does not support file import').toString(), {'textOk':M$("Ok").toString()});
            	return;
        	}
        
        	$('#fileLoader').click();
	}
    }
}

function fileImport(files) {
    $('#fileLoader').hide();
    var file = files[0];
    GLOBAL_MENU_EXPORT.currentFileName = file.name;
    $('#MrFileName').fadeOut('slow', function() {
        $(this).html('<tt>'+file.name+'</tt>');
        $(this).fadeIn('slow');
    });
    var reader  = new FileReader();
    reader.onload = function(theFile) {
        if(typeof reader.result == "string") {
            MrScheme.editor.setValue(reader.result);
            MrScheme.editor.focus();
        } else {
            apprise(M$('Unable to import file: not a text file').toString(), {'textOk':M$("Ok").toString()});
        }
    };
    reader.onerror = function(theFile) {
        apprise(M$('Unable to import file (load error)').toString(), {'textOk':M$("Ok").toString()});
    };
    
    reader.readAsText(file);
}

function fileExport(file) {
    var bb = new BlobBuilder;
    bb.append(MrScheme.editor.getValue());
    saveAs(bb.getBlob("text/plain;charset=utf-8"), file);
    MrScheme.editor.focus();
}

function menuHelp() {
    window.open("doc101/MrScheme-help-fr.html","_MrScheme_doc_fr");
}

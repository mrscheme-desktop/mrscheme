#! /bin/sh

MRSCHEME_FILES="LICENSE TODO mrscheme.html mrscheme.css mrscheme.js"

PARSER101_FILES="parser101/lexer101.js parser101/parser101.js"

EVAL101_FILES="eval101/eval101.js eval101/prims101.js eval101/tree101.js eval101/ecotree.css eval101/ecotree.js eval101/numericaltower101.js eval101/bignum101/README eval101/bignum101/biginteger101.js eval101/bignum101/schemeNumber101.js"

EDITOR101_FILES="editor101/theme101.css editor101/scheme101-mode.js \
    editor101/codemirror/src/css/baboon.png editor101/codemirror/src/css/baboon_vector.svg editor101/codemirror/src/css/docs.css \
    editor101/codemirror/src/lib/codemirror.css editor101/codemirror/src/lib/codemirror.js editor101/codemirror/src/lib/overlay.js \
    editor101/codemirror/src/lib/runmode.js"

JQUERY101_FILES="jquery101/jquery-1.6.2.js jquery101/apprise-1.5.full.js jquery101/apprise.css jquery101/tooltips.css jquery101/tooltips.js"

MEDIA101_FILES="media101/elder.png media101/ftendturn.png media101/ftfileimport.png media101/ftfileexport.png media101/ftkate.png \
    media101/ftkexi.png media101/fthelp.png"

MESSAGE101_FILES="message101/message101.js message101/messages-fr.js"

CANVAS101_FILES="canvas101/canvas101.css canvas101/images101.js canvas101/jcanvas.js"

TYPE101_FILES="type101/type101.js"

DOC101_FILES="doc101/MrScheme-help.css doc101/MrScheme-help-fr.html"

SAVE101_FILES="save101/BlobBuilder.js save101/FileSaver.js"

DATE=$(date "+%d-%m-%Y")
REVISION=$(date "+%Y%m%d")

echo "Packaging MrScheme v1.0.$REVISION"

echo $REVISION > VERSION

FILES="$MRSCHEME_FILES $PARSER101_FILES $EVAL101_FILES $EDITOR101_FILES $JQUERY101_FILES $MEDIA101_FILES $MESSAGE101_FILES $CANVAS101_FILES $TYPE101_FILES $DOC101_FILES $SAVE101_FILES VERSION"

echo "Building distribution: mrscheme-${DATE}.tar.gz"

tar czf "mrscheme-${DATE}.tar.gz" $FILES



;; Question 3

;; Utilisation de draw-line
(draw-line -1 0 1 0)
(draw-line 0 -1 0 1)

;; Utilisation de fill-triangle
(fill-triangle -1 -1 1 1 -1 1)
(fill-triangle -1 -1 0 0 -1 1)
(fill-triangle 1 -1 0 0 1 1)

;; Utilisation de draw-triangle
;; (draw-triangle -1 -0.5 -1 0.5 1 0)  ;; pas de draw-triangle  (à définir avec draw-line).

;; Utilisation de draw-ellipse
(draw-ellipse 0.0 0.0 1.0 1.0)
(draw-ellipse 0.0 0.0 0.5 1.0)

;; Utilisation de fill-ellipse
(fill-ellipse 0.0 0.0 1.0 1.0)
(fill-ellipse 0.0 0.0 1.0 0.5)


;; Utilisation de overlay
(overlay 
 (fill-triangle -1 -1 0 0 -1 1) 
 (fill-triangle 1 -1 0 0 1 1))

(overlay 
 (fill-triangle -1 -1 0 0 -1 1) 
 (fill-triangle 1 -1 0 0 1 1))

;; Question 4

(define (fill-rectangle1 x y z t)
  (overlay
   (fill-triangle x y z t x t) (fill-triangle x y z t z y)))

(define (fill-rectangle2 x y z t) 
  (overlay
   (fill-triangle x y (+ x z) (+ y t) x (+ y t)) 
   (fill-triangle x y (+ x z) (+ y t) (+ x z) y)))

(fill-rectangle1 0 0 0.45 0.75) 
(fill-rectangle2 0 0 0.45 0.75) 
(fill-rectangle1 -0.45 -0.45 0.60 0.45)
(fill-rectangle2 -0.45 -0.45 0.60 0.45)


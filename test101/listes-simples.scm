
;; Exercices simples sur les listes

;; Exercice : begaie

;;; begaie : LISTE[alpha] -> LISTE[alpha]
;;; (begaie L) rend la liste où chaque élément de L est répété
(define (begaie L)
  (if (pair? L)
      (cons (car L)
            (cons (car L)
                  (begaie (cdr L))))
      (list )))

(test (begaie (list 1 2 3 1 4)) -> (1 1 2 2 3 3 1 1 4 4))
(test (begaie (list)) -> ())

;;; debegaie : LISTE[alpha] -> LISTE[alpha]
;;; fonction telle que (debegaie (begaie L)) est égale à L
;;; HYPOTHESE : la liste donnée est bien une liste « bégayée »
(define (debegaie L)
  (if (pair? L)
      (cons (car L)
            (debegaie (cddr L)))
      (list)))

(test (debegaie (list 1 1 2 2 3 3 1 1 4 4)) -> (1 2 3 1 4))
(test (debegaie (begaie (list 1 2 3 1 4))) -> (1 2 3 1 4))

;;; debegaie-verif : LISTE[alpha] -> LISTE[alpha]
;;; fonction telle que (debegaie-verif (begaie L)) est égale à L
;;; ERREUR : lorsque la liste donnée n'est pas une liste « bégayée »
(define (debegaie-verif L)
  (if (pair? L)
      (if (and (pair? (cdr L))
               (equal? (car L) (cadr L)))
          (cons (car L) (debegaie-verif (cddr L)))
          (erreur "debegaie-verif : la liste donnée n'est pas une liste « bégayée »"))
      (list)))

(test (debegaie-verif (list 1 1 2 2 3 3 1 1 4 4)) -> (1 2 3 1 4))
(test (debegaie-verif (begaie (list 1 2 3 1 4))) -> (1 2 3 1 4))
(test (debegaie-verif (list 1 1 2 2 3 1 1 4 4)) -> ERREUR)


;; Exercice : croissante

;;; croissante? : LISTE[Nombre] -> bool
;;; (croissante? (list e1 e2 ... en)) vérifie que e1<=e2<=...<=en
(define (croissante? liste)
  (if (pair? liste)
      (if (pair? (cdr liste))
          (and (<= (car liste) (cadr liste))
               (croissante? (cdr liste)))
          #t)
      #t))

(test (croissante? (list 1 2 4 4 6 8)) -> #t)
(test (croissante? (list 1 2 5 4 8)) -> #f)
(test (croissante? (list 6)) -> #t)
(test (croissante? (list)) -> #t)

;;; croissante2? : LISTE[Nombre] -> bool
;;; (croissante2? (list e1 e2 ... en)) vérifie que e1<=e2<=...<=en
(define (croissante2? liste)
  ;; non-vide-croissante? : LISTE[Nombre]/non vide/ -> bool
  ;; (non-vide-croissante? L) vérifie que L est croissante
  (define (non-vide-croissante? L)
    (if (pair? (cdr L))
        (and (<= (car L) (cadr L))
             (non-vide-croissante? (cdr L)))
        #t))
  ;corps de croissante2?
  (if (pair? liste)
      (non-vide-croissante? liste)
      #t))

(test (croissante2? (list 1 2 4 4 6 8)) -> #t)
(test (croissante2? (list 1 2 5 4 8)) -> #f)
(test (croissante2? (list 6)) -> #t)
(test (croissante2? (list)) -> #t)

;;;  croissante-bis? : LISTE[Nombre] -> bool
;;;  (croissante-bis? (list e1 e2 ... en)) vérifie que e1<=e2<=...<=en
(define (croissante-bis? liste)
  (or (not (pair? liste))
      (not (pair? (cdr liste)))
      (and (<= (car liste) (cadr liste))
           (croissante-bis? (cdr liste)))))

(test (croissante-bis? (list 1 2 4 4 6 8)) -> #t)
(test (croissante-bis? (list 1 2 5 4 8)) -> #f)
(test (croissante-bis? (list 6)) -> #t)
(test (croissante-bis? (list)) -> #t)

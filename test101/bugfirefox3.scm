(define (launch n f)
  (define (aux n)
    (if (= n 0)
        (list)
        (cons (f) (aux (- n 1)))))
  (aux n)
  )


(define (launch-v2 n f)
  (define (aux n)
    (cond ( (= n 0) (list) )
          ( (even? n) (append (aux (/ n 2)) (aux (/ n 2))) )
          ( (odd? n)  (cons (f) (append (aux (/ (- n 1) 2)) (aux (/ (- n 1) 2)))) )
          (  else (erreur "Cas impossible : n = " n " est pair ET impair en mÃªme temps !") )
          ))
  (aux n))


(define (launch-v4 n f)
  (define (aux4 n)
    (cond ( (= n 0)                               (list) )
          ( (and (even? n) (even? (/ n 2)))       (append (append (aux4 (/ n 4)) (aux4 (/ n 4))) (append (aux4 (/ n 4)) (aux4 (/ n 4)))) )
          ( (and (even? n) (odd?  (/ n 2)))       (append (cons (f) (append (aux4 (/ (- n 2) 4)) (aux4 (/ (- n 2) 4)))) (cons (f) (append (aux4 (/ (- n 2) 4)) (aux4 (/ (- n 2) 4))))) )
          ( (and (odd?  n) (even? (/ (- n 1) 2))) (cons (f) (append (append (aux4 (/ (- n 1) 4)) (aux4 (/ (- n 1) 4))) (append (aux4 (/ (- n 1) 4)) (aux4 (/ (- n 1) 4))))) )
          ( (and (odd?  n) (odd?  (/ (- n 1) 2))) (cons (f) (append (cons (f) (append (aux4 (/ (- n 3) 4)) (aux4 (/ (- n 3) 4)))) (cons (f) (append (aux4 (/ (- n 3) 4)) (aux4 (/ (- n 3) 4)))))) )
          (  else  (erreur "ProblÃ¨me dans v4 avec n = " n) )
          ))
  (aux4 n)
  )
          
          

(define (foo)
  (random 10))




(launch 426 foo)
;(launch-v2 15000 foo)
;(launch-v4 1192 foo)





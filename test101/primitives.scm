;; tests des primitives



;;; substring

(test (substring "123456789" 2 5) -> "345")
(test (substring "123456789" 0 5) -> "12345")
(test (substring "123456789" 0 0) -> "")
(test (substring "123456789" 5 5) -> "")
(test (substring "123456789" 0 9) -> "123456789")
(test (substring "123456789" 10 12) -> ERREUR)
(test (substring "123456789" 0 10) -> ERREUR)
(test (substring "123456789" 5 3) -> ERREUR)
(test (substring "123456789" -1 4) -> ERREUR)




;; Petite démo pour la présentation LI101

;; 1) Philosophie et projet

;; Pourquoi ?
;; ==> faire évoluer le cours
;; ==> arrêt de DrScheme au profit de DrRacket qui perd sa vocation pédagogique (mais reste un environnement schemien ... pour aller plus loin)
;;     (et donc problème d'installation par les étudiants)

;; Quoi ?
;; ==> un environnement pédagogique mais pour un vrai langage : Scheme   (un sous-ensemble du langage Scheme)
;; ==> reprend les fonctionnalités de DrScheme réellement utilisées par nos étudiants  ... et en ajouter de nouvelles

;; Quand ?
;; ==> Expérimentalement en 2011/2012
;; ==> Basculement dès 2012/2013

;; 2) présentation de l'environnement

;; - Problème récurrent : installation de DrScheme et de la bibliothèque LI101
;;   Solution ==>  DrScheme sur le web  ==  MrScheme

;; - Logiciel nécessaire :   un navigateur récent,   firefox >= 3  pour profiter de toutes les fonctionnalités
;; (environnement utilisable sous chrome et safari ... pas testé sous IE et Opera).

;; a) Logo et Mr Scheme   (assistant TODO)  ==> but retrouver la "complicité" de certains étudiants avec DrScheme ("monsieur le dr scheme")
;;   ==> montrer le tooltip

;; b) Commandes :
;; Le moins possible et très focalisées... Evoquer les limites des applis client web.

;; - Nouveau programme :
;; - Importer un programme :
;; - Exporter un programme (pas hyper sympathique)
;; - Exécuter le programme

;; c) Zone d'édition :
;; - coloration syntaxique
;; - indentation assistée  (tab pour indenter la ligne courante)
;; - parenthésage assisté

(define (fact n)
  (if (= n 0)
      1
      (* n (fact (- n 1)))))

(fact 5)

(verifier fact (fact 6) -> 720)

; pour parler des tests
;; -- tester les fonctions ne consiste pas uniquement à
;; saisir une expression et attendre le résultat
;; -- il faut que l'étudiant explicite la valeur attendue
;; (ou l'erreur éventuelle)
;; -- plutôt que de le faire en commentaire, la forme
;; spéciale test est beaucoup plus efficace.

;(test (fact 6) -> 719)

;; parler des erreurs ciblées
;; - message en français et (si possible) assez détaillé

;(define (if toto) 2)

;(define (toto if) 2)

;; - si on clique sur le message d'erreur l'éditeur pointe dessus
;; - sélection en rouge de la zone incriminée
;; - .. qui ne perturbe pas l'édition   (modifier la zone d'édition)

;; et on peut tester des erreurs

(verifier fact (fact "toto") -> ERREUR)



;; d) Console : sorties d'exécution


;; e) La zone d'évaluation rapide

;; - apparaît après l'exécution d'un programme et permet des intéractions

(fact 12)

;; - attention ce n'est pas fait pour tester (car on perd ses tests)
;; mais ce n'est pas inutile pour autant.

;; - notamment quand une fonction en produit pas les bons résultats,
;; on peut tester quelques évaluations pour voir ce qui se passe...
;; pour développer MrScheme c'est très largement utilisé.

;; - si on modifie le programme, la boîte d'évaluation rapide disparaît,
;; pas de risque de tester dans un environnement désynchronisé.


;; - montrer qu'on peut agrandir la zone d'édition ou la réduire


;; 3)  Les fonctionnalités principales

;; a) support du langage scheme pour LI101 (et un peu plus...)

;; types de base : nombres (entier, réels), booléens, chaînes, symboles

2
"toto"
'toto

;; - montrer que le type s'affiche par un tooltip

;; types "complexes" :
;; - listes

(list (+ 2 2) 3 (* 5 2))
'(4 3 10)

(define (liste-des-suffixes L)
  (if (pair? L)
      (cons L (liste-des-suffixes (cdr L)))
      (list (list))))

(verifier 
 liste-des-suffixes
 (liste-des-suffixes (list 1 2 3 4)) => ((1 2 3 4) (2 3 4) (3 4) (4) ()))

;; - images

(define (sierpinski n xa ya xb yb xc yc)
  (if (= n 0)
      (fill-triangle xa ya xb yb xc yc)
      (let (
            (xmab (/ (+ xa xb) 2))
            (ymab (/ (+ ya yb) 2))
            (xmbc (/ (+ xb xc) 2))
            (ymbc (/ (+ yb yc) 2))
            (xmca (/ (+ xc xa) 2))
            (ymca (/ (+ yc ya) 2)))
        (overlay (sierpinski (- n 1) xmab ymab xb yb xmbc ymbc)
                 (overlay (sierpinski (- n 1) xmca ymca xmbc ymbc xc yc)
                          (sierpinski (- n 1) xa ya xmab ymab xmca ymca))))))

(sierpinski 5 -0.9 -0.9 0.0 0.8 0.9 -0.9)

;; - arbres binaires

(define (mon-ABR)
  (ab-noeud 66
            (ab-noeud 14
                      (ab-noeud 1 (ab-vide) (ab-vide))
                      (ab-noeud 46
                                (ab-noeud 39 (ab-vide) (ab-vide))
                                (ab-noeud 62 (ab-noeud 51 (ab-vide) (ab-vide)) (ab-noeud 63 (ab-vide) (ab-vide)))))
            (ab-noeud 69 (ab-vide) (ab-vide))))

(ab-affiche (mon-ABR))

(define (abr-coupure x ABR)
  (if (ab-noeud? ABR)
      (let ((etiq (ab-etiquette  ABR))
            (gauche (ab-gauche ABR))
            (droit (ab-droit ABR)))
        (cond ((= x etiq) (list gauche droit))
              ((< x etiq) (let ((coupe (abr-coupure x gauche)))
                               (list (car coupe)
                                     (ab-noeud etiq
                                               (cadr coupe)
                                               droit))))
              (else (let ((coupe (abr-coupure x droit)))
                         (list (ab-noeud etiq
                                         gauche
                                         (car coupe))
                               (cadr coupe))))))
      (list (ab-vide) (ab-vide))))

(define (abr-ajout-racine  x ABR)
  (let ((coupe (abr-coupure x ABR)))
       (ab-noeud x
                 (car coupe)
                 (cadr coupe))))

(define (abr-depuis-liste L)
  (if (pair? L)
      (abr-ajout-racine (car L) (abr-depuis-liste (cdr L)))
      (ab-vide)))

(let ((arbre (abr-depuis-liste (list 39 62 51 63 69 46 14 66 1))))
  (ab-affiche arbre))

;; - arbres généraux

(define (ag-A1)
  (let* ((A1 (ag-noeud "a" (list)))
         (A2 (ag-noeud "b" (list))) 
         (A3 (ag-noeud "c" (list))) 
         (A1-2 (ag-noeud "d" (list))) 
         (A2-2 (ag-noeud "e" (list))) 
         (A3-2 (ag-noeud "f" (list))) 
         (A4 (ag-noeud "u" (list A1 A2)))
         (A5 (ag-noeud "t" (list A1-2 A2-2 A3-2))))
       (ag-noeud "s" (list A1 A5 A3 A4))))

(define (ag-A2)
  (let* ((A1 (ag-noeud "c" (list)))
         (A2 (ag-noeud "g" (list))) 
         (A3 (ag-noeud "f" (list))) 
         (A4 (ag-noeud "b" (list A1)))
         (A4-2 (ag-noeud "e" (list A2)))
         (A5 (ag-noeud "d" (list A4-2))))
       (ag-noeud "a" (list A4 A5 A3))))

(ag-affiche (ag-A1))

(ag-affiche (ag-A2))

;;; ag-feuille?: ArbreGeneral[alpha] -> bool
;;;(ag-feuille? A) teste si l'arbre G est une feuille
(define (ag-feuille? A)
   (not (pair? (ag-foret A))))

;;; ag-nombre-feuilles : ArbreGeneral[alpha] -> nat
;;; (ag-nombre-feuilles A) rend le nombre de feuilles de A  
(define (ag-nombre-feuilles A)
  (if (ag-feuille? A)
      1
      (nombre-feuilles-foret (ag-foret A))))

;;; nombre-feuilles-foret : Foret[alpha]  -> nat
;;; (nombre-feuilles-foret F rend le nombre de feuilles de F
;;; HYPOTHESE : F est non vide
(define (nombre-feuilles-foret F)
  (if (pair? (cdr F))
      (+ (ag-nombre-feuilles (car F)) (nombre-feuilles-foret (cdr F)))
      (ag-nombre-feuilles (car F))))

(verifier 
 ag-nombre-feuilles 
 (ag-nombre-feuilles (ag-A1)) -> 7
 (ag-nombre-feuilles (ag-A2)) -> 3)

;; 4) Le futur

;; Probablement pour la rentrée prochaine (basculement vers MrScheme) : 

;; a) le support du typage

;; - analyse des spécifications (signature et hypothèses)
;; - vérification statique des types
;; - vérification à la volée des hypothèses

;; - listes homogènes
;; - introduction d'un type NUPLET  (et COUPLE)  distinct des listes

;; b) evaluation "omnisciente" en pas-à-pas   (stepper de DrScheme mais supportant tout li101)

;; Puis l'année suivante :

;; c) MrScheme serveur

;; - base d'exos en ligne
;; - quizs, etc.
;; - sauvegarde/soumission par les étudiants
;; - accès aux soumissions pour les enseignants


;;  D'autres idées ???




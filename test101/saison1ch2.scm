
;; Exercice 2

;;; enleve-un : Nombre -> Nombre 
;;; (enleve-un n) rend n-1 
(define (enleve-un n)
  (- n 1))

(test (enleve-un 4) -> 3)

;; Exercice 3

;;; periode : Nombre -> Nombre 
;;; (periode L) rend la période (en secondes) d’un pendule de longueur L (en cm)
(define (periode L) 
  (* 2 pi (sqrt (/ L 981))))

(periode 3) 
(periode 4)
(periode 5.2)

;; Exercice 4

;;; Fahrenheit->Celsius : Nombre -> Nombre
;;; (Fahrenheit->Celsius t) rend la température en degrés Celsius lorsque la 
;;; température est égale à "t" en degrés Fahrenheit
(define (Fahrenheit->Celsius t) 
  (* (- t 32) (/ 5 9)))


(test (Fahrenheit->Celsius 32) -> 0.0)
(test (Fahrenheit->Celsius 212) -> 100.0)
(test (Fahrenheit->Celsius 41) -> 5.0)
(test (Fahrenheit->Celsius 23) -> -5.0)
(test (Fahrenheit->Celsius 104) -> 40.0)
(test (Fahrenheit->Celsius 37.5) -> 3.055555555555556)



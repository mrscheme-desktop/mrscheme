;;; TME 1 par étudiants No 3100582 -- BENBIHI Ali

"Tests d'égalité"
(equal? "essai" "essai")
(equal? "essai" "essai")
(equal? "3" 3)
(equal? 4 5)
(equal? 4 4)
(equal? 4 (* 2 2))

"Tests d'égalité numérique"
;(= "essai" "essai"); égalité entre deux termes non numériques
(= 4 5)
(= 4 4)
(= (+ 2 2) 4)

"Tests sur number?"
(number? 45.12)
(number? -45)
(number? (+ 40 5))
(number? "essai")
(number? "3")

"Tests sur positive?"
(positive? 45.12)
(positive? -45)
(positive? (+ 40 5))
; (positive? "essai"); terme numérique attendu après positive?

"Tests sur remainder"
(remainder 10 5)
(remainder 5 10)
;(remainder 10 0); division par zÃ©ro

"Tests d'applications"
;(= 3); pas assez d'arguments
;( 3 + 2); Symbole attendu aprÃ¨s la parenthèse, opération préfixe
;( 3); Symbole attendu après la parenthèse
3

"Test d'entier"
(integer? 45.12)

"test négation"
(not #t)

"test maximum"
(max 2 3)

"test quotient"
(quotient 10 3)

"test reste"
(remainder 20 7)

"expressions avec >"
(> 2 3)
(> 3 2)

"expressions avec odd? et even?"
(odd? 3)
(even? 2)

"expressions avec +"
(+ 45 12 3)
(+ 0 0)
(+ 4 5 9)

"Opérations"
(+ (* 10 10 10) (* 9 10 10) 6)
(+ 6 (* 10 (+ 3 (* 10 (+ 9 10)))))

"Horizontale"
(draw-line -1 0 1 0)

"Verticale"
(draw-line 0 1 0 -1)

"Danette"
(fill-triangle -1 1 1 1 -1 -1)

"Pac man"
(fill-triangle 0 0 -1 1 -1 -1)

"Pac man 2"
(fill-triangle 0 0 1 1 1 -1)

"Triangle"
(overlay (draw-line -1 0.5 1 0 )
(draw-line -1 -0.5 1 0))

"Rond blanc"
(draw-ellipse -1 -1 1 1 )

"Ellipse Blanche"
(draw-ellipse -0.5 -1 0.5 1 )

"Rond Noir"
(fill-ellipse -1 -1 1 1 )

"Ellipse Noire"
(fill-ellipse -1 -0.5 1 0.5)

"Sablier"
(overlay (fill-triangle 0 0 -1 1 -1 -1) (fill-triangle 0 0 1 1 1 -1))

"Plus"
(overlay (fill-ellipse -1 -0.5 1 0.5)(fill-ellipse -0.5 -1 0.5 1 ))

"Question 4"
;;; fill-rectangle : Nombre * Nombre * Nombre * Nombre -> Image
;;; (fill-rectangle1 x y z t) produit une image carrÃ©e blanche contenant
;;; un rectangle noir

(define (fill-rectangle1 x y z t)
  (overlay
   (fill-triangle x y z t x t)
   (fill-triangle x y z t z y) ) )

;;; fill-rectangle2 : Nombre * Nombre * Nombre * Nombre -> Image
;;; (fill-rectangle2 x y z t) produit une image carrÃ©e blanche contenant
;;; un rectangle noir.

(define (fill-rectangle2 x y z t)
  (overlay
   (fill-triangle x y (+ x z) (+ y t) x (+ y t))
   (fill-triangle x y (+ x z) (+ y t) (+ x z) y) ) )

(fill-rectangle1 0 0 0.45 0.75)
(fill-rectangle2 0 0 0.45 0.75)
(fill-rectangle1 -0.45 -0.45 0.60 0.45)
(fill-rectangle2 -0.45 -0.45 0.60 0.45)

"Tangram"
;;; ()-> image
;;; (tangram) produit une image carrée blanche contenant un tangram

(define (tangram)
  (overlay
   (draw-line -1 -1 1 1)
   (draw-line 1 0 0 -1)
   (draw-line -1 1 0.5 -0.5)
   (draw-line 0.5 0.5 1 0)
   (draw-line 0.5 -0.5 -0.5 -0.5)))
(tangram)

;;; scale-draw-line : Nombre * Nombre^4 -> Image
;;; (scale-draw-line s x1 y1 x2 y2) rend l'image d'un segment
;;; d'extrémités s*x1, s*y1 s*x2 s*y2.

(define (scale-draw-line s x1 y1 x2 y2)
  (draw-line (* x1 s) (* y1 s) (* x2 s) (* y2 s)))

;;; () -> image
;;; (scale-square s) donne un carrée vide d'échelle s

(define (scale-square s)
  (overlay
   (scale-draw-line s 1 1 1 -1)
   (scale-draw-line s 1 -1 -1 -1)
   (scale-draw-line s -1 -1 -1 1)
   (scale-draw-line s -1 1 1 1)))

;;; scale-divide : Nombre -> Image
;;; (scale-divide s) divisions du tangram selon echelle

(define (scale-divide s)
  (overlay
    (scale-draw-line s -1 -1 1 1)
   (scale-draw-line s 1 0 0 -1)
   (scale-draw-line s -1 1 0.5 -0.5)
   (scale-draw-line s 0.5 0.5 1 0)
   (scale-draw-line s 0.5 -0.5 -0.5 -0.5)))

;;; scale-tangram : Nombre -> Image
;;; (scale-tangram s) donne un tangram à l'echelle s
(define (scale-tangram s)
  (overlay
   (scale-square s)
   (scale-divide s)))

"Tests"
(scale-tangram 0.9)
(scale-tangram 0.5)
(scale-tangram 0.3)




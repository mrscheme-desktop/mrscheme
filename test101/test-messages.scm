;; this is for testing the error messages, uncomment at will

(define (plus a b)
  (+ a b))

(plus 1 2)

;(plus 1)

;(plus 1 2 3)

;(define (fois a b a)
;  (* a b a))

;(fois 1 2 3)

;(define (fois a b c)
;  (define (aux a b)
;    (* a b))
;  (define (aux b c)
;    (* b c))
;  ;; corps
;  (* (aux a b) (aux b c)))


; toto

;(1 2 3)

; ((+ 1 2) 2 3)


;(let ((a 1)
;      (b 2)
;      (a 3))
;  (+ a b))


;(let* ((a 1)
;       (b 2)
;       (a 3))
;  (+ a b))

;(quotient)
;(quotient 1)
;(quotient 1 2 3)

;(equal? + +)
;(equal? + 2)
;(equal? plus plus)
;(equal? plus 2)
;(equal? 2 plus)
;(equal? + plus)
;(equal? plus +)

; (positive? "toto")

; (odd? "toto")
; (odd? 2.4)
;(even? "toto")
;(even? 2.4)

;(= "toto" 2)
;(> "toto" 3)

;(/ 2 "toto")
;(/ 2 0)

;(sqrt "toto")

;(- 3 "toto")

;(max 3 4 "toto")

;(remainder "toto" 2)
;(remainder 2 "toto")
;(remainder 2 0)

;(quotient "toto" 2)
;(quotient 2 "toto")
;(quotient 2 0)

;(car "toto")
;(car (list))
;(cdr "toto")

;(caar "toto")
;(caar (list 1 2))

;(cddar 12)
;(cddar (list 1 2))
;(cddar (list (list 1) 2))
(cddar (list (list 1 2) 3))

;(random "toto")
;(random -3)
;(random 0)
















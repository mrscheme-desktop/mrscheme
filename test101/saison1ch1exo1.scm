;; Question 1

"Tests d'égalité"
(test (equal?  "essai" "essai") -> #t) 
(test (equal? "essai" "essai ") -> #f)
(test (equal? "3" 3) -> #f)
(test (equal? 4 5)  -> #f)
(test (equal? 4 4) -> #t)
(test (equal? 4 (* 2 2)) -> #t)

"Tests d'égalité numérique"
(test (= "essai" "essai") -> ERREUR)
(test (= 4 5) -> #f)
(test (= 4 4) -> #t)
(test (= (+ 2 2) 4) -> #t)

"Tests sur number?"
(test (number? 45.12) -> #t)
(test (number? -45) -> #t)
(test (number? (+ 40 5)) -> #t)
(test (number? "essai") -> #f)
(test (number? "3") -> #f)

"Tests sur positive?"
(test (positive? 45.12) -> #t)
(test (positive? -45) -> #f)
(test (positive? (+ 40 5)) -> #t)
(test (positive? "essai") -> ERREUR)

"Tests sur remainder"
(test (remainder 10 5) -> 0)
(test (remainder 5 10) -> 5)
(test (remainder 10 0) -> ERREUR)

"Tests d'applications"
(test (= 3) -> ERREUR)
;;(test (3 + 2) -> ERREUR)  ;; erreur d'analyse syntaxique ?
;; (test (3) -> ERREUR) ;; idem
(test 3 -> 3)


;; Question 2

;; fonction : integer? (predicat), arg = 45.12
(test (integer? 45.12) -> #f)
;; fonction : not,  arg = #t
(test (not #t) -> #f)
;; fonction : max,  arg = 2 3
(test (max 2 3) -> 3)
;; fonction : quotient, arg 10 3
(test (quotient 10 3) -> 3)
;; fonction : remainder, arg 20 7
(test (remainder 20 7) -> 6)
;; expressions avec >
(test (> 23 34.5) -> #f)
(test (> 11 2.3) -> #t)
;; applications avec odd? et even?
(test (odd? 2) -> #f)
(test (odd? 3) -> #t)
(test (even? 2) -> #t)
(test (even? 3) -> #f)
;; applications avec + et un nombre variable d'arguments
;; (test (+) -> 0) ;;  au moins 2 arguments maintenant
;; (test (+ 1) -> 1)  ;; idem
(test (+ 2 3) -> 5)
(test (+ 12 23 45) -> 80)


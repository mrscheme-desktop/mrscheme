var TT = {
 delay: 600,
 count : 0,

 setTips: function() {
    
    $('.button , .value , #MrLogoSpan').hover(function(evt) {
        if(TT.ttframe==null) {
          TT.ttframe = $('<div id="tooltip-frame"></div>');
          TT.ttframe.appendTo('body');
        }

        if(TT.ttframe.css('display')=='block') {
          TT.ttframe.hide();
        }

        // store the tooltip being hovered
        var tooltip = $(this).data('tooltip');
        if(tooltip==null) {
          tooltip = $(this).find(".tooltip");
            //console.log("tooltip",tooltip);
          if(tooltip==null) {
            throw "No tooltip (please report)";
          }
          $(this).data('tooltip',tooltip);
          tooltip.show();
          tooltip.remove();
        }
        tooltip.appendTo('#tooltip-frame');
        TT.ttframe.css('top', (evt.pageY - 10) + 'px')
          .css('left', (evt.pageX + 20) + 'px');

       var count = TT.count;
       TT.timer = setTimeout(function(){
           if(count==TT.count) {
             TT.ttframe.fadeIn('fast');
           }
          }, TT.delay);
      }, function() {
        TT.count++;
        // on mouseout, make tooltip disappear
        clearTimeout(TT.timer);
        var tooltip = $(this).data('tooltip');
        if(tooltip!=null) {
          TT.ttframe.fadeOut('fast', function() {
              tooltip.remove(); // TODO: do not remove here but replace child (race condition)
            });
        }
      }).mousemove(function(evt) {
          TT.ttframe.css('top', (evt.pageY - 10) + 'px')
            .css('left', (evt.pageX + 20) + 'px');  
        });
  }
}


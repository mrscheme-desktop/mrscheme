
var Message101 = {};

Message101.Translations = {};
Message101.Language = "en";

Message101.Translate = function(lang,key,trans) {
  var transTbl = Message101.Translations[key];
  if(transTbl == null) {
    transTbl = {};
    Message101.Translations[key] = transTbl;
  }
  transTbl[lang] = trans;
};

Message101.Message = function(msg,args) {
  this.msg = msg;
  if(args==null) {
    this.args = new Array();
  } else if(args instanceof Array) { // TODO: test array
      //console.log("args is array",args);
    this.args = args;
  } else {
    this.args = new Array(args);
  }
  
  this.toString = function() {
    var tmsg = this.msg;
    var transTbl = Message101.Translations[this.msg];
    if(transTbl!=null) {
      tmsg = transTbl[Message101.Language];
      if(tmsg == null) {
        tmsg = this.msg;
      }
    }
    // now produce the correct string
    var str = "";
    var param = false;
    for(var i=0;i<tmsg.length;i++) {
      var next = tmsg.charAt(i);
      if(next=='$') {
        if(param) {
          str += '$';
          param = false;
        } else {
          param = true;
        }
      } else if(next>='0' && next<='9') {
        if(param) {
            var ref = parseInt(next);
            //console.log("arg",ref,"value",this.args[ref],this.args);
            str += this.args[ref];
            param = false;
        } else {
          str += next;
        }
      } else {
        str += next;
      }
    }
    return str;
  }
};

var M$ = function(msg,args) {
  transTbl = Message101.Translations[msg];
  if(transTbl==null) {
    // register key
    transTbl = {};
    transTbl['default'] = msg;
    Message101.Translations[msg] = transTbl;
  }
  return new Message101.Message(msg,args);
};


